let collection = [];

// Write the queue functions below.

function print() {
	return collection;
}

function enqueue(element) {
  	const queueLength = collection.length;
    collection[queueLength] = element;
    return collection;
}

function dequeue() {
  const firstElement = collection[0];
    for (let i = 0; i < collection.length - 1; i++) {
      collection[i] = collection[i + 1];
    }
    collection.length--;
    return collection;
}

function front() {
    return collection[0];
}

function size() {
  let count = 0;
  for (let i = 0; i < collection.length; i++) {
    count++;
  }
  return count;
}

function isEmpty() {
  let empty = true;
    for (let i = 0; i < collection.length; i++) {
      if (collection[i] !== undefined) {
        empty = false;
        break;
      }
    }
    return empty;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
